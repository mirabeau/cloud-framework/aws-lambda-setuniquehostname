#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function

import boto3
import logging
import json
import os
from pprint import pformat, pprint
import random
import sys
import time

'''

Set Unique Hostname

This Lambda function triggers on autoscaling group notification/lifecycle hooks to create a (hopefully) unique Route53 entry per instance.
For example, a machine started with role "ecs" and slice "staging" might get roleid 1 and end up with ecs-staging-1.$domainname in route53.
When terminated this name and roleid is freed again.

This lambda is influenced by the following ENVIRONMENT variables (usually set when deploying the lambda):
===
LogLevel                 : Changes the loglevel, default is INFO, other options are DEBUG, WARNING, ERROR
TagPrefix                : Prefix used for custom tags set to AWS resources
RoleTag                    : EC2 tag to READ role name from, defaults to "mcf:role"
RoleIDTag                : EC2 tag to SET and READ role ID from, defaults to "mcf:role"
SliceTag                 : EC2 tag to READ slice name from, defaults to "mcf:slicename". If you don't use slices, set to empty string and it will be skipped.
StackTag                 : EC2 tag to READ stack name from, defaults to "mcf:environment"
HostnameTag            : EC2 tag to SET with calculated hostname (i.e. what this script does), defaults to "mcf:hostname"
HostedzoneNameTag: EC2 tag to READ hosted zone name from to use for route53 registration, defaults to "mcf:internal_dnsname".
HostedzoneIDTag    : EC2 tag to READ hosted zone ID from to use for route53 registration, defaults to "mcf:internal_hostedzone_id".
IPAddressTag         : EC2 tag to READ/WRITE ip address to for route53 deregistration, defaults to "mcf:internal_ipv4_address"
ASGLock1Tag            : ASG Tag to READ/WRITE in order to make sure this script has no race condition issues
ASGLock2Tag            : ASG Tag to READ/WRITE in order to make sure this script has no race condition issues
===

    Theory of locking operation:
    Since this lambda can be invoked by multiple instances at the "same" time from the same ASG (i.e. when starting 10 instances at the same time) it needs to handle this somewhat gracefully.
    The way we try this is by abusing the ASG tags:

    1 . read ASG tag 'islocked'
    2 . while islocked exists (sleep 0.1s), re-read
         - if islocked timestamp is outdated, clear it, retrigger lambda
         - if this takes more than 1s, retrigger lambda
    3 . if 'islocked' is free, check if 'wantlock' is also free
    4 . while wantlock exists (sleep 0.1s), re-read
         - if this takes more than 1s, retrigger lambda
    5 . write instance-id|timestamp to wantlock
    6 . reread 5, if instance-id mismatch on wantlock we had a race condition, retrigger lambda (and let other process 'win')
    7 . write instance-id|newtimestamp to islocked
    8 . reread 7, if instance-id mismatch: (unexpected), retrigger lambda (maybe: check wantlock)
    9 . clear wantlock tag
    10. # Critical section -- do the route53 magicery
    11. clear locked tag

    Yes, this might suffer from starvation, but we are not running this function all the time, only when instances are started/stopped.
    The expiration time for the lock is set to 30 seconds, if we can't finish in that time we have other issues.
    (while testing the lock applied within a second, unlock is even faster)

'''


class LockException(Exception):
    '''Raise when lock could not be obtainer, not really an error, just retry needed'''


class ASGLock:
    def __init__(self, instanceId, asgName, lockTag, wantLockTag):
        self.asg = boto3.client('autoscaling')
        self.asgGroupName = asgName
        self.instanceId = instanceId
        self.lockTag = lockTag
        self.wantLockTag = wantLockTag

    def __enter__(self):
        # Try to get lock
        res = self.lock_asg()
        if res:
            return self
        raise LockException('Could not get lock')

    def __exit__(self, type, value, tb):
        # Get rid of locks we hold
        self.lock_unlock(self.lockTag)
        self.lock_unlock(self.wantLockTag)
        return True

    def lock_get_asg_tag(self, tag):
        paginator = self.asg.get_paginator('describe_tags')
        try:
            ltags = paginator.paginate(
                Filters=[
                    {'Name': 'auto-scaling-group', 'Values': [self.asgGroupName]},
                    {'Name': 'key', 'Values': [tag]},
                ]
            ).build_full_result()['Tags']
        except Exception as e:
            logging.warning("[lock_get_asg_tag] ERROR FETCHING TAG {} on ASG {}: {}".format(tag, self.asgGroupName, pformat(e)))
            return "**--EXCEPTION[{}]--**".format(pformat(e))
        if len(ltags) > 0:
            logging.debug("Found tag {} with value {} for ASG {}".format(tag, ltags[0]['Value'], self.asgGroupName))
            return ltags[0]['Value']
        logging.debug("Could not find tag {} for ASG {}".format(tag, self.asgGroupName))
        return None

    def lock_set_asg_tag(self, tag, value):
        try:
            res = self.asg.create_or_update_tags(
                Tags=[{
                    'Key': tag,
                    'Value': value,
                    'PropagateAtLaunch': False,
                    'ResourceId': self.asgGroupName,
                    'ResourceType': 'auto-scaling-group',
                }],
            )
        except Exception as e:
            logging.warning("[lock_set_asg_tag] WARNING: Tried to update tags but it failed: {}".format(pformat(e)))
            raise LockException('Could not get lock')
            return False
        return True

    def lock_create_tag(self):
        return "{}|{}".format(self.instanceId, time.time())

    def lock_parse_tag(self, tag):
        instanceId, ts = tag.split('|')
        tsdelta = time.time() - float(ts)
        if tsdelta > 30:
            logging.debug("Tag has expired, tsdelta {}".format(tsdelta))
        return [instanceId, bool(tsdelta > 30)]

    def lock_unlock(self, tag, isExpired=False):
        if isExpired:
            logging.info("Unlocking lock {} due to lock expiration!".format(tag))
        else:
            logging.debug("Unlocking lock {}".format(tag))
        self.asg.delete_tags(
            Tags=[{
                'ResourceId': self.asgGroupName,
                'ResourceType': 'auto-scaling-group',
                'Key': tag,
                # Wuh, value on delete?
                # 'Value': 'string',
                'PropagateAtLaunch': False  # Wuh, another weird one
            }]
        )

    def lock_dolock(self, tag):
        ts = time.time()
        value = self.lock_create_tag()
        logging.debug("Locking tag {} on ASG {} and instance {} with ts {} -> [{}]".format(tag, self.asgGroupName, self.instanceId, ts, value))
        return self.lock_set_asg_tag(tag, value)

    def lock_asg(self):
        logging.debug("Trying to get lock on ASG {} for instance {}".format(self.asgGroupName, self.instanceId))
        # 1. get islocked tag
        islocked = self.lock_get_asg_tag(self.lockTag)
        if islocked is not None:
            # Someone has the lock, see if it's us, otherwise reschedule.
            instanceId, expired = self.lock_parse_tag(islocked)
            if expired:
                logging.warning("[lock_asg] WARNING: Lock expired! Resetting and retriggering!")
                self.lock_unlock(self.lockTag, True)
                return False
            if instanceId == self.instanceId:
                logging.warning("[lock_asg] WARNING: Lock was already locked by us -- will continue, but this should not happen! Relocking and continueing")
                return self.lock_dolock(self.lockTag)
            # Ok, it's locked by someone else, wait.
            # Optional: sleep
            time.sleep(0.1)
            return False
        # 3 . if 'islocked' is free, check if 'wantlock' is also free
        wantlock = self.lock_get_asg_tag(self.wantLockTag)
        if wantlock is not None:
            # Someone has the lock, see if it's us, otherwise reschedule.
            instanceId, expired = self.lock_parse_tag(wantlock)
            if expired:
                logging.warning("[lock_asg] WARNING: Want Lock Tag expired! Resetting and retriggering!")
                self.lock_unlock(self.wantLockTag, True)
                return False
            if instanceId == self.instanceId:
                logging.warning("[lock_asg] WARNING: Lock was already locked by us -- will continue, but this should not happen! Relocking and continueing")
                if not self.lock_dolock(self.wantLockTag):
                    return False
            else:
                # Ok, it's locked by someone else, wait, retrigger.
                time.sleep(0.1)
                return False
        # Wantlock is free, grab it and confirm we have it.
        haveWantLock = self.lock_dolock(self.wantLockTag)
        time.sleep(random.random())  # Random wait for collision handling
        wantlock = self.lock_get_asg_tag(self.wantLockTag)
        if wantlock is None:
            # Ayeieeee
            logging.info("Failed to grab wantlock after it should be mine - retry later")
            return False
        instanceId, expired = self.lock_parse_tag(wantlock)
        if instanceId != self.instanceId:
            logging.info("Wantlock failed to secure, retry later.")
            return False
        # Wantlock is ours, check that lock is still free and grab it
        islocked = self.lock_get_asg_tag(self.lockTag)
        if islocked is not None:
            logging.info("What the, lock is grabbed even though we grabbed wantlock?! Unlocking and bailing")
            self.lock_unlock(self.wantLockTag)
            return False
        # Still free, grab it!
        mylock = self.lock_dolock(self.lockTag)
        return mylock


class UniqueHostTagger:
    def __init__(self):
        self.route53 = boto3.client('route53')
        self.asg = boto3.client('autoscaling')
        self.ec2 = boto3.resource('ec2')
        self.configure_tags()

    def configure_logger(self, loglevel=logging.INFO, logfile=None):
        logger = logging.getLogger()
        logger.setLevel(loglevel)
        format = "[%(filename)30s:%(funcName)20s():%(lineno)4s:%(levelname)-7s] %(message)s"
        if logfile and logfile is not None:
            logging.basicConfig(level=loglevel,
                                filename=logfile,
                                filemode='w',
                                format=format)
        else:
            logging.basicConfig(level=loglevel,
                                format=format)
        logging.getLogger('boto3').setLevel(logging.WARNING)
        logging.getLogger('botocore').setLevel(logging.WARNING)

    def get_instance_info(self, ec2_object):
        tag_dict = {}
        if ec2_object.tags is None:
            return None
        for tag in ec2_object.tags:
            tag_dict[tag['Key']] = tag['Value']
        return tag_dict

    def get_asg_instance_roleids(self, asgName):
        response = self.asg.describe_auto_scaling_groups(AutoScalingGroupNames=[asgName], MaxRecords=1)
        if 'AutoScalingGroups' not in response or not len(response['AutoScalingGroups']):
            logging.debug("No ASG with name %s found" % (asgName))
            return None
        roleIds = []
        asg_object = response['AutoScalingGroups'][0]
        for instance in asg_object['Instances']:
            ec2_object = self.ec2.Instance(instance['InstanceId'])
            instance_tags = self.get_instance_info(ec2_object)
            if (ec2_object.state['Name'] == 'running' or ec2_object.state['Name'] == 'pending') and self.tags['roleidTag'] in instance_tags:
                roleIds.append(int(instance_tags[self.tags['roleidTag']]))
        return roleIds

    def del_route53_a_record(self, hostedZoneId, ec2Hostname, ipaddress):
        # AWS API sucks again here. It is simply impossible to 'Get the record called X'.
        # Instead, you can filter on Name and Type, and they will return a sorted list of records, so we need to filter ourselves.
        try:
            # First check if the resource exists and matches.
            response = self.route53.list_resource_record_sets(
                HostedZoneId=hostedZoneId,
                StartRecordName=ec2Hostname,
                StartRecordType='A',
                MaxItems="10",
            )
        except Exception as e:
            logging.error("Error fetching Route53 A Record info for hostedzone[%s] ec2Hostname[%s] (and ipaddress[%s]): %s" % (hostedZoneId, ec2Hostname, ipaddress, pformat(e)))
            return None
        records = filter(lambda record: record['Name'] == ec2Hostname or record['Name'] == ec2Hostname + '.', response['ResourceRecordSets'])

        # Compare result with supplied ip
        if len(records) == 0:
            # No records? No action needed.
            logging.info("No records found - nothing to delete for hostedzone[%s] ec2Hostname[%s] (and ipaddress[%s]), raw response was %s" % (hostedZoneId, ec2Hostname, ipaddress, pformat(response)))
            return 0
        elif ipaddress is None:
            # If we don't know the active ip address just check the current one if it's a running instance in this ASG
            # TODO: make this better. We need to fetch the old IP somewhere.... maybe tag it on creation.
            logging.info("Do not know old IP address for instance --> A record for hostedzone[%s] hostname[%s] IP [%s] will be deleted." % (hostedZoneId, ec2Hostname, records[0]['ResourceRecords'][0]['Value']))
            ipaddress = records[0]['ResourceRecords'][0]['Value']
            # Check ASG

        elif len(records) != 1 or records[0]['ResourceRecords'][0]['Value'] != str(ipaddress):
            logging.info("Not deleting A record for hostedzone[%s] hostname[%s] because given IP [%s] does not match actual ip [%s] -- already updated before we could delete." % (hostedZoneId, ec2Hostname, ipaddress, records[0]['ResourceRecords'][0]['Value']))
            return None

        # Seems to match, go and delete it.
        try:
            response = self.route53.change_resource_record_sets(
                HostedZoneId=hostedZoneId,
                ChangeBatch={
                    'Comment': 'delete %s (%s)' % (ec2Hostname, ipaddress),
                    'Changes': [{
                        'Action': 'DELETE',
                        'ResourceRecordSet': {
                            'Name': ec2Hostname,
                            'Type': 'A',
                            'TTL': 300,
                            'ResourceRecords': [{'Value': ipaddress}]
                        }
                    }]
                })
        except Exception as e:
            logging.error("Error deleting Route53 A Record on hostedzone[%s] ec2Hostname[%s] and ipaddress[%s]: %s" % (hostedZoneId, ec2Hostname, ipaddress, pformat(e)))
            print(e)

    def getEnv(self, name, default):
        if name in os.environ:
            return os.environ[name]
        else:
            return default

    def get_hostname(self, tag_dict, roleId):
        # We have something like mcf:environment=ota | mcf:role=ecs | mcf:slicename=staging | mcf:internal_dnsname=internaldomain.staging
        logging.debug("Get hostname with roleId %s and tag_dict: %s" % (pformat(tag_dict), str(roleId)))
        if not self.tags['stackTag'] in tag_dict:
            tag_dict[self.tags['stackTag']] = ''
        if not self.tags['roleTag'] in tag_dict:
            logging.error("Could not find roleTag[%s] in tag_dict, aborting. tag_dict: %s" % (self.tags['roleTag'], pformat(tag_dict)))
            return None
        if not self.tags['sliceTag'] in tag_dict:
            tag_dict[self.tags['sliceTag']] = ''
        if not self.tags['hostedzoneNameTag'] in tag_dict:
            logging.error("Could not find hostedzoneNameTag[%s] in tag_dict, aborting. tag_dict: %s" % (self.tags['hostedzoneNameTag'], pformat(tag_dict)))
            return None
        ec2Hostname = None
        try:
            if len(tag_dict[self.tags['sliceTag']]) > 0:
                ec2Hostname = '%s-%s%d.%s' % (tag_dict[self.tags['sliceTag']], tag_dict[self.tags['roleTag']], roleId, tag_dict[self.tags['hostedzoneNameTag']])
            elif len(tag_dict[self.tags['stackTag']]) > 0:
                ec2Hostname = '%s-%s%d.%s' % (tag_dict[self.tags['stackTag']], tag_dict[self.tags['roleTag']], roleId, tag_dict[self.tags['hostedzoneNameTag']])
            else:
                ec2Hostname = '%s%d.%s' % (tag_dict[self.tags['roleTag']], roleId, tag_dict[self.tags['hostedzoneNameTag']])
        except Exception as e:
            logging.error("Exception: Could not craft hostname: %s" % (pformat(e)))
            return None
        logging.debug("Returning from get_hostname with: %s" % (ec2Hostname))
        return ec2Hostname

    def set_route53_a_record(self, hostedZoneId, ec2Hostname, ipaddress):
        try:
            response = self.route53.change_resource_record_sets(
                HostedZoneId=hostedZoneId,
                ChangeBatch={
                    'Comment': 'add %s -> %s' % (ec2Hostname, ipaddress),
                    'Changes': [{
                        'Action': 'UPSERT',
                        'ResourceRecordSet': {
                            'Name': ec2Hostname,
                            'Type': 'A',
                            'TTL': 300,
                            'ResourceRecords': [{'Value': ipaddress}]
                        }
                    }]
                })
        except Exception as e:
            logging.error("Error setting Route53 A Record on hostedzone[%s] ec2Hostname[%s] and ipaddress[%s]: %s" % (hostedZoneId, ec2Hostname, ipaddress, pformat(e)))
            print(e)

    def unclaim_role_id(self):
        ec2_object = self.ec2.Instance(self.instanceId)
        try:
            with ASGLock(self.instanceId, self.asgGroupName, self.tags['ASGLock1Tag'], self.tags['ASGLock2Tag']):
                if not ec2_object:
                    logging.warning("[unclaim_role_id] Fail - No EC2 instance with id %s found" % (self.instanceId))
                    return None
                # Figure out claimed record
                tag_dict = self.get_instance_info(ec2_object)
                if not tag_dict:
                    logging.info("[unclaim_role_id] Fail - Could not get tags for EC2 instance %s" % (self.instanceId))
                    return 0
                if not self.tags['roleidTag'] in tag_dict:
                    logging.info("[unclaim_role_id] Fail - Instance does not have role id tag [%s] - not unclaiming. tag_dict: %s" % (self.tags['roleidTag'], pformat(tag_dict)))
                    return 0
                roleId = tag_dict[self.tags['roleidTag']]
                logging.debug("Instance has role id tag [%s] set to %s" % (self.tags['roleidTag'], str(roleId)))

                if self.tags['hostnameTag'] in tag_dict:
                    ec2Hostname = tag_dict[self.tags['hostnameTag']]
                    logging.info("[unclaim_role_id] Instance hostname from tag %s: %s -- unclaiming" % (self.tags['hostnameTag'], str(ec2Hostname)))
                else:
                    ec2Hostname = self.get_hostname(tag_dict, roleId)
                    logging.info("[unclaim_role_id] Instance hostname is calculated to %s -- unclaiming" % (str(ec2Hostname)))
                if ec2Hostname is None:
                    logging.info("[unclaim_role_id] Fail - Instance hostname is None -- can't unclaim?!")
                    return None
                ipaddress = ec2_object.private_ip_address
                if self.tags['ipaddressTag'] in tag_dict:
                    ipaddress = tag_dict[self.tags['ipaddressTag']]
                self.del_route53_a_record(tag_dict[self.tags['hostedzoneIdTag']], ec2Hostname, ipaddress)
                ec2_object.delete_tags(
                    Tags=[
                        {'Key': self.tags['roleidTag'], 'Value': str(roleId)},
                        {'Key': self.tags['ipaddressTag'], 'Value': str(ipaddress)},
                        {'Key': self.tags['hostnameTag'], 'Value': ec2Hostname}
                    ]
                )
                logging.info("[unclaim_role_id] Success - Instance %s [%s] unclaimed role id %s with IP %s" % (self.instanceId, ec2Hostname, str(roleId), ec2_object.private_ip_address))
        except LockException as le:
            logging.debug("Did not get lock [{}] -- spinning".format(pformat(e)))
            self.retrigger_lambda()
        except Exception as e:
            logging.warning("[unclaim_role_id] Fail - Exception trying to get lock [{}] -- TROUBLE.".format(pformat(e)))

    def claim_role_id(self):
        roleId = 0
        lastId = 0
        ec2_object = self.ec2.Instance(self.instanceId)
        if not ec2_object:
            logging.warning("[claim_role_id] Fail - No EC2 instance with id %s found" % (self.instanceId))
            return None
        tag_dict = self.get_instance_info(ec2_object)
        if not tag_dict:
            logging.warning("[claim_role_id] Fail - Could not get tags for EC2 instance %s" % (self.instanceId))
            return 0
        if self.tags['roleidTag'] in tag_dict:
            logging.info("[claim_role_id] Fail - Instance already has role id %s" % (tag_dict[self.tags['roleidTag']]))
            return 1
        if 'aws:autoscaling:groupName' not in tag_dict:
            logging.info("[claim_role_id] Fail - Instance {} has no 'aws:autoscaling:groupName' tag -- why. We know ASG {}".format(self.instanceId, self.asgGroupName))
            return 0
        try:
            with ASGLock(self.instanceId, self.asgGroupName, self.tags['ASGLock1Tag'], self.tags['ASGLock2Tag']):
                roleIds = self.get_asg_instance_roleids(tag_dict['aws:autoscaling:groupName'])
                logging.debug("Claimd roleIds are: %s" % (pformat(roleIds)))
                for idx, val in enumerate(sorted(roleIds)):
                    lastId = val
                    if val == 0 or val > idx + 1:
                        roleId = idx + 1
                        break
                if roleId == 0:
                    roleId = lastId + 1
                ec2Hostname = self.get_hostname(tag_dict, roleId)
                if ec2Hostname is None:
                    logging.warning("[claim_role_id] Fail - Could not calculate hostname?!")
                    return None
                logging.debug("Found free role id %d, will set %s tag and %s tag for EC2 instance %s" % (roleId, self.tags['roleidTag'], self.tags['hostnameTag'], self.instanceId))
                ec2_object.create_tags(
                    Tags=[
                        {'Key': self.tags['roleidTag'], 'Value': str(roleId)},
                        {'Key': self.tags['ipaddressTag'], 'Value': str(ec2_object.private_ip_address)},
                        {'Key': self.tags['hostnameTag'], 'Value': ec2Hostname}
                    ]
                )
                # Update Route53
                self.set_route53_a_record(tag_dict[self.tags['hostedzoneIdTag']], ec2Hostname, ec2_object.private_ip_address)
                logging.info("[claim_role_id] Success - Instance %s [%s] claimed role id %s with IP %s" % (self.instanceId, ec2Hostname, str(roleId), ec2_object.private_ip_address))
                return 1
        except LockException as le:
            logging.debug("Did not get lock [{}] -- spinning".format(pformat(e)))
            self.retrigger_lambda()
        except Exception as e:
            logging.warning("[claim_role_id] Fail - Exception trying to get lock [{}] -- TROUBLE.".format(pformat(e)))

    def complete_lifecycle(self, lifecycleHookName, instanceId, asgName, lchResult='CONTINUE'):
        response = None
        try:
            response = self.asg.complete_lifecycle_action(
                LifecycleHookName=lifecycleHookName,
                AutoScalingGroupName=asgName,
                LifecycleActionResult=lchResult,
                InstanceId=instanceId
            )
        except Exception as e:
            logging.info("Failed trying to complete lifecycle action on lch[%s] asg[%s] result[%s] instanceid[%s]: %s" % (lifecycleHookName, asgName, lchResult, instanceId, pformat(e)))
            return False
        logging.info("Completed lifecycle [%s] asg[%s] instanceid[%s] result[%s], response: %s" % (lifecycleHookName, asgName, instanceId, lchResult, pformat(response)))
        return True

    def handle(self, event, context):
        logging.info("Lambda received the event {} with context {}".format(pformat(event), pformat(context)))
        self.event = event
        self.context = context
        self.snsmsg, self.message, self.instanceId, self.asgGroupName, self.snsArn, self.TopicArn = None, None, None, None, None, None
        if 'instance-id' in event:
            logging.info("Found instance-id in event, claiming role for instance %s", pformat(event['instance-id']))
            claim_role_id(event['instance-id'])
        elif 'autoscaling:TEST_NOTIFICATION' in event:
            logging.info("Got autoscaling:TEST_NOTIFICATION in event -- ignoring.")
            return {}
        elif 'Records' in event and len(event['Records']) > 0 and 'Sns' in event['Records'][0]:
            try:
                self.snsmsg = self.event['Records'][0]['Sns']['Message']
                self.message = json.loads(self.snsmsg)
                if 'Event' in self.message and self.message["Event"] == 'autoscaling:TEST_NOTIFICATION':
                    logging.info("Got autoscaling:TEST_NOTIFICATION in event's SNS message -- ignoring.")
                    return {}
                self.instanceId = self.message['EC2InstanceId']
                self.asgGroupName = self.message['AutoScalingGroupName']
                self.snsArn = self.event['Records'][0]['EventSubscriptionArn']
                self.TopicArn = self.event['Records'][0]['Sns']['TopicArn']
            except Exception as e:
                logging.error("Could not handle supplied event, error was: %s", pformat(e))
                return None
            logging.debug("Records : %s", self.event['Records'][0])
            logging.debug("SNS         : %s", self.event['Records'][0]['Sns'])
            logging.debug("Message : %s", self.message)
            logging.debug("Instance: %s", self.instanceId)
            logging.debug("ASG         : %s", self.asgGroupName)
            logging.debug("SNS ARN : %s", self.snsArn)

            # See why we were called
            if 'LifecycleTransition' in self.message.keys():
                logging.debug("LifeCycleHook[%s] has LifecycleTransition Message from Autoscaling: %s", self.message['LifecycleHookName'], self.message['LifecycleTransition'])
                lifecycleHookName = self.message['LifecycleHookName']
                if self.message['LifecycleTransition'].find('autoscaling:EC2_INSTANCE_TERMINATING') > -1:
                    logging.info("Instance terminating -- deregister hostname.")
                    self.unclaim_role_id()
                    complete_lifecycle(lifecycleHookName, self.instanceId, self.asgGroupName, lchResult='CONTINUE')
                elif self.message['LifecycleTransition'].find('autoscaling:EC2_INSTANCE_LAUNCHING') > -1:
                    logging.info("Instance spinning up -- register hostname.")
                    self.claim_role_id()
                    complete_lifecycle(lifecycleHookName, self.instanceId, self.asgGroupName, lchResult='CONTINUE')
                else:
                    logging.info("We don't care about this message for now.")
            if 'Event' in self.message.keys():
                if self.message['Event'].find('autoscaling:EC2_INSTANCE_TERMINATE') > -1:
                    logging.info("Instance terminating -- deregister hostname.")
                    self.unclaim_role_id()
                elif self.message['Event'].find('autoscaling:EC2_INSTANCE_LAUNCH') > -1:
                    logging.info("Instance spinning up -- register hostname.")
                    self.claim_role_id()
                else:
                    logging.info("We don't care about this message for now.")
            else:
                logging.warning("[handle] Fail - Event reason not found in message, keys were: %s" % (pformat(self.message.keys())))
            return {}
        else:
            logging.warning("[handle] Fail - Could not handle event, key information missing!")
            return {}

    def retrigger_lambda(self):
        time.sleep(random.uniform(2, 5))  # Wait between 2 and 5 secs
        try:
            if 'Reinvoked' in self.event:
                self.event['Reinvoked'] = self.event['Reinvoked'] + 1
                if self.event['Reinvoked'] > 300:
                    logger.warning("Reinvoke count 300 -- aborting this call!")
                    sys.exit(0)
            else:
                self.event['Reinvoked'] = 1
        except Exception as e:
            logging.warning("Error in reinvoke count checker: {}".format(pformat(e)))
            pass
        logging.warning("[retrigger_lambda] Re-triggering lambda function (reinvocation count {}) and exiting!".format(self.event['Reinvoked']))
        self.lam = boto3.client('lambda')
        self.lam.invoke(
            FunctionName=self.context.function_name,
            Payload=json.dumps(self.event),
            InvocationType='Event',
            # ClientContext='string',
        )
        sys.exit(0)

    def configure_tags(self):
        self.tags = {}
        tag_prefix = self.getEnv("TagPrefix", "mcf")
        self.tags['hostnameTag'] = self.getEnv("HostnameTag", tag_prefix + ":hostname")
        self.tags['hostedzoneIdTag'] = self.getEnv("HostedzoneIDTag", tag_prefix + ":internal_hostedzone_id")
        self.tags['roleidTag'] = self.getEnv("RoleIDTag", tag_prefix + ":roleid")
        self.tags['stackTag'] = self.getEnv("StackTag", tag_prefix + ":stack")
        self.tags['roleTag'] = self.getEnv("RoleTag", tag_prefix + ":role")
        self.tags['sliceTag'] = self.getEnv("SliceTag", tag_prefix + ":slicename")
        self.tags['hostedzoneNameTag'] = self.getEnv("HostedzoneNameTag", tag_prefix + ":internal_dnsname")
        self.tags['ipaddressTag'] = self.getEnv("IPAddressTag", tag_prefix + ":internal_ipv4_address")
        self.tags['ASGLock1Tag'] = self.getEnv("ASGLock1Tag", tag_prefix + ":islocked")
        self.tags['ASGLock2Tag'] = self.getEnv("ASGLock2Tag", tag_prefix + ":wantlock")
        logging.debug("Tags configured as follows: %s" % (pformat(self.tags)))


def lambda_handler(event, context):
    uht = UniqueHostTagger()
    loglevel = os.environ['LogLevel'] if 'LogLevel' in os.environ else logging.INFO
    uht.configure_logger(loglevel)
    return uht.handle(event, context)


# Used for testing

'''

Event looks like this:

{
    'Records': [
     {'EventSource': 'aws:sns',
        'EventSubscriptionArn': 'arn:aws:sns:eu-west-1:271644199767:EC2UniqueHostname:9c75788d-d49a-4c75-aefb-8a5b7753d3ef',
        'EventVersion': '1.0',
        'Sns': {
            'Message': '{
                    "Progress":50,
                    "AccountId":"271644199767",
                    "Description":"Launching a new EC2 instance: i-0f3234069628fc5e0",
                    "RequestId":"61a58961-dc53-c188-aeae-4dc98f9f6391",
                    "EndTime":"2018-03-24T14:44:43.673Z",
                    "AutoScalingGroupARN":"arn:aws:autoscaling:eu-west-1:271644199767:autoScalingGroup:4e3f1840-8b52-43d0-8670-32b62f1fdca1:autoScalingGroupName/otap-management-services-ecs-ECSInstanceAsg-1GUZ249PG6VMA",
                    "ActivityId":"61a58961-dc53-c188-aeae-4dc98f9f6391",
                    "StartTime":"2018-03-24T14:43:41.936Z",
                    "Service":"AWS Auto Scaling",
                    "Time":"2018-03-24T14:44:43.673Z",
                    "EC2InstanceId":"i-0f3234069628fc5e0",
                    "StatusCode":"InProgress",
                    "StatusMessage":"",
                    "Details":{
                        "Subnet ID":"subnet-ed4fe6b6",
                        "Availability Zone":"eu-west-1b"
                    },
                    "AutoScalingGroupName":"otap-management-services-ecs-ECSInstanceAsg-1GUZ249PG6VMA",
                    "Cause":"At 2018-03-24T14:43:41Z a difference between desired and actual capacity changing the desired capacity, increasing the capacity from 2 to 3.",
                    "Event":"autoscaling:EC2_INSTANCE_LAUNCH"
                }',
            'MessageAttributes': {},
            'MessageId': '9fb3739c-dd71-551c-9685-20acbbea22f4',
            'Signature': 'Igg+rYhZ6Qw4Cwrtzqkw2FmmyG0cKRJ6JZ32T2xBnE1gPdUtv+zf9LBsHVs8VjYprnzQJsLOg9kQRu5tnkhQ9hKgLH3jLMr0X9SO6mgzbDDDABobHjTNs/MvEzLZ6xLL2Ou1aqq6rM6KHrWZodTzb800/Ezw7ST32ik0RJIW4ad5syXHnNbbj9jKrWOh+twUWyABYxm40/3juKXPI8QdRJAeKz5TW7gewIJrc0PjK/P3+ZyCWQ78ecNL90Z487Mm8if/RG2b6VzE7SOc+NOki1QaUDPtZk1JY+hZqCyF0CXxmxdUfEQ7vKGpeXfu9EIVpavuc7O8YabZP3N0suQiyg==',
            'SignatureVersion': '1',
            'SigningCertUrl': 'https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-433026a4050d206028891664da859041.pem',
            'Subject': 'Auto Scaling: launch for group "otap-management-services-ecs-ECSInstanceAsg-1GUZ249PG6VMA"',
            'Timestamp': '2018-03-24T14:44:43.785Z',
            'TopicArn': 'arn:aws:sns:eu-west-1:271644199767:EC2UniqueHostname',
            'Type': 'Notification',
            'UnsubscribeUrl': 'https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:271644199767:EC2UniqueHostname:9c75788d-d49a-4c75-aefb-8a5b7753d3ef'
            }
        }
    ]
}

'''


if __name__ == "__main__":
    e = {
        'instance-id': 'i-0d01ecb70f6d323b0',
        'AutoScalingGroupName': 'otap-management-services-ecs-ECSInstanceAsg-1GUZ249PG6VMA'
    }
    c = {}
    # Commandline call, Call lambda handler
    # lambda_handler(e, c)

    # ASG Lock test
    tags = {
        'ASGLock1Tag': 'mcf:islocked',
        'ASGLock2Tag': 'mcf:wantlock',
    }
    logger = logging.getLogger()
    loglevel = logging.DEBUG
    logger.setLevel(loglevel)
    format = "[%(filename)30s:%(funcName)20s():%(lineno)4s:%(levelname)-7s] %(message)s"
    logging.basicConfig(level=loglevel, format=format)
    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('botocore').setLevel(logging.WARNING)
    try:
        with ASGLock(e['instance-id'], e['AutoScalingGroupName'], tags['ASGLock1Tag'], tags['ASGLock2Tag']):
            logging.info("Locked -- would claim/unclaim roles here, sleeping 30s so you can check out the tags etc....")
            time.sleep(30)
    except Exception as e:
        logging.info("Exception[{}] - would retrigger lambda".format(pformat(e)))
        sys.exit(1)
    logging.info("Should be unlocked again.")
